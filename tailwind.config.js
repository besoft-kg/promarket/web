const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};

module.exports = {
  theme: {
    fontFamily: {
      'sans': ['Roboto'],
    },
    colors: {
      ...defaultTheme.colors,
      primary: '#009846',
      secondary: '#ef7f1a',
    },
  }
};
