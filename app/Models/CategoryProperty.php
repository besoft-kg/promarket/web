<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryProperty extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
        'position',
        'category_group_id',
        'category_id',
    ];
}
