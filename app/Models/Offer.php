<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Offer extends Model
{
  use HasFactory;

  protected $hidden = [
    'store_id',
    'created_at',
    'updated_at',
    'product_id',
  ];

  protected $with = [
    'store',
    'product',
  ];

  protected $dates = [
    'discount_start_on',
    'discount_end_on',
  ];

  protected $appends = [
    'branches',
  ];

  protected $casts = [
    'discount_start_on' => 'datetime:Y-m-d',
    'discount_end_on' => 'datetime:Y-m-d',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->select(['*', DB::raw(
        'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, price - ((price * discount_percentage) / 100), price)) as sale_price,' .
        'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, 1, 0)) as discount_active'
      )]);
    });
  }

  public function store()
  {
    return $this->hasOne(IStore::class, 'id', 'store_id');
  }

  public function product()
  {
    return $this->hasOne(XOfferProduct::class, 'id', 'product_id');
  }

  public function getBranchesAttribute()
  {
    return Branch::whereRaw(DB::raw(
      'id IN(SELECT branch_id FROM tabekg_offer_branches WHERE offer_id = ?)'
    ), [$this->id])->get();
  }
}
