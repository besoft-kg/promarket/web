<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductArticle extends Model
{
    use HasFactory;

    protected $hidden = [
        'product_id',
        'position',
        'created_at',
        'updated_at',
        'identity',
    ];
}
