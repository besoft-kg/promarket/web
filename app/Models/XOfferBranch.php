<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XOfferBranch extends Model
{
    use HasFactory;

    protected $table = 'branches';

    protected $hidden = [
        'updated_at',
        'created_at',
        'creator_id',
        'laravel_through_key',
        'store_id',
    ];

    protected $casts = [
        'schedule' => 'array',
    ];
}
