<?php

namespace App\Models\Product\Offer;

use App\Models\IImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
  use HasFactory;

  protected $table = 'stores';

  protected $with = [
    'logo',
  ];

  protected $hidden = [
    'logo_id',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'logo_id'
      ));
    });
  }

  public function logo() {
    return $this->hasOne(IImage::class, 'id', 'logo_id');
  }
}
