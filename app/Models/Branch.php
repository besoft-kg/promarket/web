<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;

    protected $hidden = [
        'updated_at',
        'created_at',
        'creator_id',
        'laravel_through_key',
    ];

    protected $casts = [
        'schedule' => 'array',
    ];
}
