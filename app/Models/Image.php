<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $casts = [
        'dimensions' => 'array',
        'path' => 'array',
        'size' => 'array',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id',
    ];
}
