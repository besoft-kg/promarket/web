<?php

namespace App\Models\IOffer;

use App\Models\IImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
  use HasFactory;

  protected $table = 'products';

  protected $appends = [
    'main_image',
  ];

  protected $hidden = [
    'parent_id',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'parent_id'
      ));
    });
  }

  public function getMainImageAttribute()
  {
    return IImage::whereRaw(DB::raw(
      'id IN(SELECT image_id FROM tabekg_product_images WHERE product_id = ? OR product_id = ?)'
    ), [$this->parent_id, $this->id])->first();
  }
}
