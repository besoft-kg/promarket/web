<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
  use HasFactory;

  protected $with = [
    'logo',
    'branches',
  ];

  protected $casts = [
    'contacts' => 'array',
  ];

  protected $appends = [
    'offers',
  ];

  protected $hidden = [
    'logo_id',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'contacts,' .
        'logo_id,' .
        'about',
      ));
    });
  }

  public function logo()
  {
    return $this->hasOne(IImage::class, 'id', 'logo_id');
  }

  public function getOffersAttribute()
  {
    return \App\Models\Store\Offer::whereRaw(DB::raw(
      'store_id = ? AND ' .
      'EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id)'
    ), [$this->id])->get();
  }

  public function branches()
  {
    return $this->hasMany(XStoreBranch::class, 'store_id', 'id');
  }
}
