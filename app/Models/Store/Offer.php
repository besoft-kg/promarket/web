<?php

namespace App\Models\Store;

use App\Models\Store\Offer\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Offer extends Model
{
  use HasFactory;

  protected $table = 'offers';

  protected $hidden = [
    'store_id',
    'product_id',
  ];

  protected $with = [
    'product',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'price,' .
        'used,' .
        'discount_percentage,' .
        'store_id,' .
        'product_id,' .
        'IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, TRUE, FALSE) as discount_active,' .
        'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, price - ((price * discount_percentage) / 100), price)) as sale_price'
      ));
    });
  }

  public function product()
  {
    return $this->hasOne(Product::class, 'id', 'product_id');
  }
}
