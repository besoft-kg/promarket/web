<?php

namespace App\Models\Store\Offer;

use App\Models\IImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
  use HasFactory;

  protected $table = 'products';

  protected $appends = [
    'main_image',
  ];

  protected $hidden = [
    'parent_id',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'parent_id, ' .
        '(SELECT COUNT(id) FROM tabekg_offers WHERE EXISTS(SELECT 1 FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id) AND (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE parent_id = tabekg_products.id))) as offers_count'
      ));
    });
  }

  public function getMainImageAttribute()
  {
    return IImage::whereRaw(DB::raw(
      'id IN(SELECT image_id FROM tabekg_product_images WHERE product_id = ? OR product_id = ?)'
    ), [$this->parent_id, $this->id])->first();
  }
}
