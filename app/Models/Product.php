<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
  use HasFactory;

  protected $with = [
    'brief_info',
    'articles',
    'images',
    'children',
    'brand',
    'colors',
  ];

  protected $hidden = [
    'brand_id',
  ];

  protected $appends = [
    'cheapest_offer',
    'modifications',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'description,' .
        'categories_path,' .
        'category_id,' .
        'brand_id,' .
        '(SELECT COUNT(id) FROM tabekg_offers WHERE EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id) AND (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE parent_id = tabekg_products.id))) as offers_count'
      ));
    });
  }

  public function getOffersAttribute()
  {
    return \App\Models\Product\Offer::whereRaw(
      '(product_id IN(SELECT id FROM tabekg_products WHERE parent_id = ?) OR product_id = ?) AND ' .
      'EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id)'
      , [$this->id, $this->id]
    )
      ->orderByRaw('sale_price ASC')
      ->get();
  }

  public function getCheapestOfferAttribute()
  {
    return \App\Models\Product\Offer::whereRaw(
      '(product_id IN(SELECT id FROM tabekg_products WHERE parent_id = ?) OR product_id = ?) AND ' .
      'EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id)'
      , [$this->id, $this->id]
    )
      ->orderByRaw('sale_price ASC')
      ->first();
  }

  public function brand()
  {
    return $this->hasOne(IBrand::class, 'id', 'brand_id')->with('logo');
  }

  public function images()
  {
    return $this->hasMany(ProductImage::class, 'product_id', 'id');
  }

  public function colors()
  {
    return $this->hasManyThrough(Color::class, ProductColor::class, 'product_id', 'id', 'id', 'color_id');
  }

  public function articles()
  {
    return $this->hasMany(ProductArticle::class, 'product_id', 'id');
  }

  public function getModificationsAttribute()
  {
    if ($this->parent_id !== null) return [];

    $items = CategoryProperty::whereRaw(DB::raw(
      'id IN(SELECT property_id FROM tabekg_product_values WHERE product_id = ? AND (payload = "" OR payload IS NULL))',
    ), [$this->id])->get();

    $result = [];
    foreach ($items as $item) {
      if ($item->type === 'select') $item['options'] = CategoryOptions::select('id', 'value')->whereRaw('id IN(SELECT payload FROM tabekg_product_values WHERE property_id = ? AND product_id IN(SELECT id FROM tabekg_products WHERE parent_id = ?))', [$item->id, $this->id])->get();
      elseif ($item->type === 'integer') $item['options'] = ProductValue::whereRaw(DB::raw(
        'product_id IN(SELECT id FROM tabekg_products WHERE parent_id = ?) AND property_id = ?'
      ), [$this->id, $item->id])->pluck('payload')->toArray();
      array_push($result, $item);
    }
    return $result;
  }

  public function children()
  {
    return $this->hasMany(Product::class, 'parent_id', 'id');
  }

  public function brief_info()
  {
    return $this->hasMany(ProductBriefInfo::class, 'product_id', 'id');
  }
}
