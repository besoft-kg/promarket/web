<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
        'position',
        'image_id',
        'product_id',
    ];

    protected $with = [
        'image'
    ];

    public function image() {
        return $this->hasOne(IImage::class, 'id', 'image_id');
    }
}
