<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchQuery extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_ip',
        'query',
    ];

    protected $hidden = [
        'user_id',
        'user_ip',
        'created_at',
        'updated_at',
    ];
}
