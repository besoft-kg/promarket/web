<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IImage extends Model
{
    use HasFactory;

    protected $table = 'images';

    protected $casts = [
        'dimensions' => 'array',
        'path' => 'array',
        'size' => 'array',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id',
        'name',
        'hash',
        'laravel_through_key',
    ];
}
