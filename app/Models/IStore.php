<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IStore extends Model
{
  use HasFactory;

  protected $table = 'stores';

  protected $casts = [
    'contacts' => 'array'
  ];

  protected $with = [
    'logo',
  ];

  protected $hidden = [
    'logo_id',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,'.
        'logo_id,'.
        'about,'.
        'contacts',
      ));
    });
  }

  public function logo()
  {
    return $this->hasOne(IImage::class, 'id', 'logo_id');
  }
}
