<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $with = [
        'icon',
    ];

    protected $casts = [
      'all_subcategories' => 'array',
    ];

    public function icon() {
        return $this->hasOne(IImage::class, 'id', 'icon_id');
    }

    public function children() {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function getProductsAttribute() {
        return IProduct::whereIn('category_id', $this->all_subcategories ?: [$this->id])
            ->whereNull('parent_id')->orderBy('offers_count', 'desc')->limit(6)->get();
    }

    protected $hidden = [
        'created_at',
        'creator_id',
        'icon_id',
        'popularity',
        'updated_at',
    ];
}
