<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IOffer extends Model
{
  use HasFactory;

  protected $table = 'offers';

  protected $with = [
    'store',
    'product',
  ];

  protected $hidden = [
    'store_id',
    'product_id',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'price,' .
        'used,' .
        'store_id,' .
        'product_id,' .
        'discount_percentage,' .
        'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, price - ((price * discount_percentage) / 100), price)) as sale_price,' .
        'FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, 1, 0)) as discount_active'
      ));
    });
  }

  public function store()
  {
    return $this->hasOne(\App\Models\IOffer\Store::class, 'id', 'store_id');
  }

  public function product()
  {
    return $this->hasOne(\App\Models\IOffer\Product::class, 'id', 'product_id');
  }
}
