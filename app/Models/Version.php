<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
  protected $hidden = [
    'created_at',
    'updated_at',
    'platform',
    'service',
  ];

  protected $casts = [
    'data' => 'array',
    'published_on' => 'datetime',
  ];
}
