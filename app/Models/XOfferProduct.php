<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class XOfferProduct extends Model
{
  use HasFactory;

  protected $table = 'products';

  protected $with = [
    'brief_info',
    'articles',
    'brand',
  ];

  protected $hidden = [
    'updated_at',
    'created_at',
    'description',
    'creator_id',
    'brand_id',
  ];

  protected $appends = [
    'colors',
    'main_image',
    'images',
  ];

  public function brand()
  {
    return $this->hasOne(Brand::class, 'id', 'brand_id');
  }

  public function getImagesAttribute()
  {
    return ProductImage::where('product_id', $this->parent_id)->orWhere('product_id', $this->id)->get();
  }

  public function getColorsAttribute()
  {
    return Color::whereRaw(
      DB::raw('id IN(SELECT color_id FROM tabekg_product_colors WHERE product_id = ?)'),
      [$this->id]
    )->get();
  }

  public function articles()
  {
    return $this->hasMany(ProductArticle::class, 'product_id', 'parent_id');
  }

  public function brief_info()
  {
    return $this->hasMany(ProductBriefInfo::class, 'product_id', 'parent_id');
  }

  public function getMainImageAttribute()
  {
    return IImage::whereRaw(DB::raw(
      'id IN(SELECT image_id FROM tabekg_product_images WHERE product_id = ? OR product_id = ?)'
    ), [$this->parent_id, $this->id])->first();
  }
}
