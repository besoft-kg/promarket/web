<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductBriefInfo extends Model
{
    use HasFactory;

    protected $table = 'product_brief_info';

    protected $hidden = [
        'product_id',
        'position',
    ];
}
