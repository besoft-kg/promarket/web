<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IProduct extends Model
{
  use HasFactory;

  protected $table = 'products';

  protected $with = [
    'main_image',
  ];

  protected $hidden = [
    'brand_id',
  ];

  protected $appends = [
    'cheapest_offer',
  ];

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'brand_id,' .
        '(SELECT COUNT(id) FROM tabekg_offers WHERE EXISTS(SELECT 1 FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id) AND (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE parent_id = tabekg_products.id))) as offers_count'
      ));
    });
  }

  public function getCheapestOfferAttribute()
  {
    return DB::table('offers')
      ->selectRaw(DB::raw('id, price, discount_percentage, IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, TRUE, FALSE) as discount_active, FLOOR(IF(discount_percentage IS NOT NULL AND CURRENT_DATE BETWEEN discount_start_on AND discount_end_on, price - ((price * discount_percentage) / 100), price)) as sale_price'))
      ->whereRaw('product_id IN(SELECT id FROM tabekg_products WHERE parent_id = ?) OR product_id = ?', [$this->id, $this->id])
      ->whereRaw(DB::raw('EXISTS(SELECT 1 FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id)'))
      ->orderByRaw('sale_price ASC')
      ->first();
  }

  public function main_image()
  {
    return $this->hasOneThrough(IImage::class, ProductImage::class, 'product_id', 'id', 'id', 'image_id');
  }
}
