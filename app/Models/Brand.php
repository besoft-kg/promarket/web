<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Brand extends Model
{
  use HasFactory;

  public function logo()
  {
    return $this->hasOne(IImage::class, 'id', 'logo_id');
  }

  protected $with = [
    'logo',
  ];

  public function products()
  {
    return $this->hasMany(IProduct::class, 'brand_id', 'id')->whereNull('parent_id')->orderBy('offers_count', 'desc');
  }

  public static function booted()
  {
    static::addGlobalScope(function ($builder) {
      $builder->selectRaw(DB::raw(
        'id,' .
        'title,' .
        'logo_id'
      ));
    });
  }

  protected $hidden = [
    'logo_id',
  ];
}
