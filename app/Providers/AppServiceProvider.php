<?php

namespace App\Providers;

use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    DB::listen(function ($query) {
      Log::info(
        $query->sql,
        $query->bindings,
        $query->time
      );
    });

    View::share('main_categories', Category::whereNull('parent_id')->whereRaw(DB::raw(
      'EXISTS(SELECT 1 FROM tabekg_products WHERE category_id = tabekg_categories.id OR JSON_CONTAINS(tabekg_categories.all_subcategories, CONVERT(tabekg_products.category_id, CHAR), \'$\'))'
    ))->get());
  }
}
