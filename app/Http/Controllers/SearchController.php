<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\IBrand;
use App\Models\IProduct;
use App\Models\SearchQuery;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SearchController extends Controller
{
    public function get_prompt(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'query' => 'bail|string|required',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        $query = $request->get('query');

        return SearchQuery::groupBy('query')
            ->selectRaw('sum(total) as sum_total, query')
            ->where('query', 'like', $query . '%')
            ->orderBy('sum_total', 'desc')
            ->limit(10)->get();
    }

    public function get_result(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'query' => 'bail|string',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        $query = $request->get('query');
        $query_words = explode(' ', $query);
        $ip = $request->ip();

        $result = [
            'products' => IProduct::whereNull('parent_id')->where(function($q) use ($query, $query_words){
                $q->where('title', 'like', '%' . $query . '%');
                foreach ($query_words as $s) {
                    $q->orWhereRaw('id IN(SELECT product_id FROM tabekg_product_keywords WHERE text = ?)', [$s]);
                }
            })->get(),
            'categories' => Category::where(function($q) use ($query, $query_words){
                $q->where('title', 'like', '%' . $query . '%');
                foreach ($query_words as $s) {
                    $q->orWhereRaw('id IN(SELECT category_id FROM tabekg_category_keywords WHERE text = ?)', [$s]);
                }
            })->get(),
            'brands' => IBrand::where('title', 'like', '%' . $query . '%')->get(),
            'stores' => Store::where('title', 'like', '%' . $query . '%')->get(),
        ];

        $search_query = SearchQuery::where('user_ip', $ip)->where('query', $query)->first();

        if ($search_query) {
            $search_query->total++;
            $search_query->save();
        } else {
            SearchQuery::create([
                'user_ip' => $request->ip(),
                'query' => $query,
            ]);
        }

        return $result;
    }
}
