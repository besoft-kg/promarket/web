<?php

namespace App\Http\Controllers;

use App\Models\CategoryGroup;
use App\Models\CategoryOptions;
use App\Models\Product;
use App\Models\ProductValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function get_index(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|required|integer',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        return Product::findOrFail($request->get('id'))->append('offers');
    }

    public function get_specification(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|required|integer',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        $item = Product::findOrFail($request->get('id'));

        $items = CategoryGroup::with('properties')->where('category_id', $item->category_id)->get()->toArray();

        $items = array_map(function($i) use ($item) {
            $i['properties'] = array_map(function($g) use ($item) {
                $value = ProductValue::where('product_id', $item->id)->where('property_id', $g['id'])->first();

                if ($value) {
                    switch ($g['type']) {
                        case 'select':
                            $v = CategoryOptions::find($value->payload);
                            $g['value'] = $v ? $v->value : null;
                            break;
                        case 'integer':
                            $g['value'] = (int) $value->payload;
                            break;
                        case 'multi_select':
                            $v = CategoryOptions::whereIn('id', json_decode($value->payload))->pluck('value')->toArray();
                            $g['value'] = $v;
                            break;
                    }
                } else $g['value'] = null;

                return $g;
            }, $i['properties']);
            return $i;
        }, $items);

        return $items;
    }
}
