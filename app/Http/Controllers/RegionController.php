<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    public function get_index(Request $request)
    {
        return Region::all();
    }
}
