<?php

namespace App\Http\Controllers;

use App\Models\Version;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class VersionController extends Controller
{
  public function get_last(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'platform' => 'bail|required|string|in:android,web,ios',
      'service' => 'bail|string',
    ]);

    if ($validator->fails()) {
      return response([
        'payload' => $validator->getMessageBag(),
        'message' => 'Неверные параметры!',
        'result' => 'invalid_params'
      ], 400);
    }

    return Version::whereNotNull('published_on')
      ->where('service', $request->has('service') ? $request->get('service') : null)
      ->where('platform', $request->get('platform'))->orderBy('version_code', 'desc')
      ->limit(1)->first();
  }
}
