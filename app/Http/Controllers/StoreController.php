<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    public function get_index(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|required|integer',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        return Store::findOrFail($request->get('id'));
    }
}
