<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OfferController extends Controller
{
    public function get_index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|integer|required',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        return Offer::findOrFail($request->get('id'));
    }

    public function get_product(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|integer|required',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        return Offer::findOrFail($request->get('id'));
    }
}
