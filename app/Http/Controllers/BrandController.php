<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    public function get_index(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|integer',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        if (!$request->has('id')) {
          return Brand::all();
        }

        return Brand::with('products')->findOrFail($request->get('id'));
    }
}
