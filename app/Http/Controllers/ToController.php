<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\IBrand;
use App\Models\IOffer;
use App\Models\IProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ToController extends Controller
{
  public function get_home(Request $request)
  {
    return [
      'categories' => Category::all(),
      'products' => [
        'by_categories' => (function () {
          $categories = Category::whereNull('parent_id')->whereRaw(DB::raw(
            'EXISTS(SELECT 1 FROM tabekg_products WHERE category_id = tabekg_categories.id OR JSON_CONTAINS(tabekg_categories.all_subcategories, CONVERT(tabekg_products.category_id, CHAR), \'$\'))'
          ))->get()->toArray();
          foreach ($categories as $index => $category) {
            $categories[$index]['products'] = IProduct::whereNull('parent_id')->whereRaw(DB::raw(
              'JSON_CONTAINS(?, CONVERT(category_id, CHAR))'
            ), [json_encode($category['all_subcategories'])])->limit(10)->orderBy('offers_count', 'desc')->get()->toArray();
          }
          return $categories;
        })(),
        'popular' => IProduct::whereNull('parent_id')
          ->whereRaw(DB::raw(
            'EXISTS(SELECT id FROM tabekg_offers WHERE (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE tabekg_products.id = p.parent_id)) AND EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id))'
          ))
          ->limit(10)
          ->orderByRaw('RAND()')
          ->get(),
        'discount' => IProduct::whereNull('parent_id')->whereRaw(DB::raw(
          'EXISTS(SELECT id FROM tabekg_offers WHERE (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE tabekg_products.id = p.parent_id)) AND (CURRENT_DATE BETWEEN discount_start_on AND discount_end_on) AND EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id))'
        ))->limit(10)->orderBy('id', 'desc')->get(),
      ],
      'offers' => [
        'only_today' => IOffer::whereRaw(DB::raw(
          'discount_end_on = CURRENT_DATE AND EXISTS(SELECT 1 FROM tabekg_offer_branches WHERE offer_id = tabekg_offers.id)'
        ))->get(),
      ],
      'brands' => [
        'popular' => IBrand::whereRaw(DB::raw(
          'EXISTS(SELECT 1 FROM tabekg_products WHERE brand_id = tabekg_brands.id)'
        ))->orderBy('popularity', 'desc')->limit(10)->get(),
      ],
      'sections' => [
//        [
//          'title' => 'Защитите себя от коронавируса',
//          'payload' => [IProduct::find(13)],
//          'type' => 'products'
//        ],
//        [
//          'title' => 'ProMarket рекомендует',
//          'payload' => [IProduct::find(7)],
//          'type' => 'products'
//        ]
      ],
    ];
  }

}
