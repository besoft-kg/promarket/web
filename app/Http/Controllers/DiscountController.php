<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\IProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DiscountController extends Controller
{
  public function get_index(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'category_id' => 'bail|integer',
    ]);

    if ($validator->fails()) {
      return response([
        'payload' => $validator->getMessageBag(),
        'message' => 'Неверные параметры!',
        'result' => 'invalid_params'
      ], 400);
    }

    $query = IProduct::whereNull('parent_id');

    if ($request->has('category_id')) {
      $category = Category::find($request->get('category_id'));
      $query->whereIn('category_id', $category->all_subcategories ?: [$category->id]);
    }

    return $query->whereExists(function ($query) {
      $query->select(DB::raw(1))
        ->from('offers')
        ->where(function ($q) {
          $q->whereColumn('offers.product_id', 'products.id')
            ->orWhereRaw('tabekg_offers.product_id in(SELECT id FROM tabekg_products as p WHERE parent_id = tabekg_products.id)');
        })
        ->whereRaw(DB::raw(
          'EXISTS(SELECT 1 FROM tabekg_offer_branches WHERE offer_id = tabekg_offers.id) AND discount_percentage IS NOT NULL'
        ))
        ->whereDate('discount_start_on', '<=', Carbon::now())
        ->whereDate('discount_end_on', '>=', Carbon::now());
    })->limit(20)->get();
  }
}
