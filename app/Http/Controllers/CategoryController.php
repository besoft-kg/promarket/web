<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\IProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function get_index(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'bail|required|integer',
        ]);

        if ($validator->fails()) {
            return response([
                'payload' => $validator->getMessageBag(),
                'message' => 'Неверные параметры!',
                'result' => 'invalid_params'
            ], 400);
        }

        $item = Category::with(['children'])->findOrFail($request->get('id'));

        return response([
          'item' => $item,
          'products' => IProduct::whereIn('category_id', $item->all_subcategories ?: [$item->id])
            ->whereNull('parent_id')->orderBy('offers_count', 'desc')->limit(30)->get(),
        ]);
    }
}
