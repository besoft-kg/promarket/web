<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SupportController extends Controller
{
  public function post_support(Request $request) {
    $validator = Validator::make($request->all(), [
      'text' => 'bail|required|string',
      'phone_number' => 'bail|required|string',
    ]);

    if ($validator->fails()) {
      return response([
        'payload' => $validator->getMessageBag(),
        'message' => 'Неверные параметры!',
        'result' => 'invalid_params'
      ], 400);
    }

    $url = 'https://api.telegram.org/bot' . env('TELEGRAM_BOT_TOKEN') . '/' . 'sendMessage';

    $text = $request->get('text');
    $phone_number = $request->get('phone_number');

    $data_string = json_encode([
      'chat_id' => env('TELEGRAM_SUPPORT_CHANNEL_ID'),
      'text' => "<strong>Новая заявка:</strong>\nТекст: $text\nТелефон номер: <a href='tel:$phone_number'>$phone_number</a>",
      'parse_mode' => 'HTML',
    ]);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    $result = json_decode(curl_exec($ch));

    if ($result->ok) {
      return response(['status' => 'success']);
    } else {
      return response(['status' => 'error'], 500);
    }
  }
}
