<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ffffff">
  <link rel="canonical" href="https://promarket.kg/">
  <meta name="application-name" content="ProMarket">

  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="theme-color" content="#ffffff">
  <meta property="og:site_name" content="ProMarket">

  <meta name="description"
        content="@yield('meta_description', 'ProMarket поможет найти и купить подходящий товар по выгодной цене.')">
  <meta property="og:type" content="@yield('meta_type', 'website')">
  <meta property="og:title" content="@yield('meta_title', 'Выбор и покупка товаров из проверенных магазинов')">
  <meta property="twitter:title" content="@yield('meta_title', 'Выбор и покупка товаров из проверенных магазинов')">
  <meta property="og:description" content="@yield('meta_description', 'ProMarket поможет найти и купить подходящий товар по выгодной цене.')">
  <meta name="twitter:description" content="@yield('meta_description', 'ProMarket поможет найти и купить подходящий товар по выгодной цене.')">
  <meta property="og:url" content="{{\Illuminate\Support\Facades\URL::current()}}">
  <meta name="keywords" content="@yield('meta_keywords', 'Оптом Кыргызстан дешево скидки акции цена розница доставка на заказ интернет магазин Бишкек Ош promarket.kg характеристики фото')">

  <meta property="og:image" content="@yield('meta_image', '/assets/logo.png')" />
  <meta property="og:image:secure_url" content="@yield('meta_image', '/assets/logo.png')" />
  <meta property="og:image:type" content="@yield('meta_image_type', 'image/png')" />
  <meta property="og:image:width" content="@yield('meta_image_width', '512')" />
  <meta property="og:image:height" content="@yield('meta_image_height', '512')" />
  <meta property="og:image:alt" content="@yield('meta_title', 'Выбор и покупка товаров из проверенных магазинов')" />

  <title>@yield('meta_title', 'ProMarket: Выбор и покупка товаров из проверенных магазинов')</title>

  @yield('meta_tags')

  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;600;700&display=swap" rel="stylesheet">
  <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="ProMarket">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  @include('layouts/header')

  <div class="container mx-auto lg:pt-32 xl:pt-30 pt-3">

    @yield('content')

  </div>

  @include('layouts/footer')

  <script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
