<footer class="bg-primary text-white mt-2">
  <div class="container mx-auto p-4">
    <div class="grid sm:grid-cols-1 md:grid-cols-2 xl:grid-cols-3 lg:grid-cols-3">
      <div>
        <h5 class="text-xl my-2">PROMARKET</h5>
        <p>ProMarket — каталог описаний и цен на бытовую и компьютерную технику, электронику, товары для дома, для офиса и многое другое.</p>
        <div class="mt-1">
          <!-- WWW.NET.KG , code for http://promarket.kg -->
          <script language="javascript" type="text/javascript">
            java="1.0";
            java1=""+"refer="+escape(document.referrer)+"&amp;page="+escape(window.location.href);
            document.cookie="astratop=1; path=/";
            java1+="&amp;c="+(document.cookie?"yes":"now");
          </script>
          <script language="javascript1.1" type="text/javascript">
            java="1.1";
            java1+="&amp;java="+(navigator.javaEnabled()?"yes":"now");
          </script>
          <script language="javascript1.2" type="text/javascript">
            java="1.2";
            java1+="&amp;razresh="+screen.width+'x'+screen.height+"&amp;cvet="+
              (((navigator.appName.substring(0,3)=="Mic"))?
                screen.colorDepth:screen.pixelDepth);
          </script>
          <script language="javascript1.3" type="text/javascript">java="1.3"</script>
          <script language="javascript" type="text/javascript">
            java1+="&amp;jscript="+java+"&amp;rand="+Math.random();
            document.write("<a href='https://www.net.kg/stat.php?id=6839&amp;fromsite=6839' target='_blank'>"+
              "<img src='https://www.net.kg/img.php?id=6839&amp;"+java1+
              "' border='0' alt='WWW.NET.KG' width='31' height='31' /></a>");
          </script>
          <noscript>
            <a href='https://www.net.kg/stat.php?id=6839&amp;fromsite=6839' target='_blank'><img
                src="https://www.net.kg/img.php?id=6839" border='0' alt='WWW.NET.KG' width='31'
                height='31' /></a>
          </noscript>
          <!-- /WWW.NET.KG -->

        </div>
      </div>

      <div>
        <h5 class="text-xl my-2">Полезные ссылки</h5>

        <ul>
          <li>
            <a href="/about" class="text-white">О нас</a>
          </li>
          <li>
            <a href="/terms" class="text-white">Пользовательское соглашение</a>
          </li>
          <li>
            <a href="/privacy" class="text-white">Политика конфиденциальности</a>
          </li>
        </ul>
      </div>

      <div>
        <h5 class="text-xl my-2">Наши приложения</h5>

        <ul>
          <li>
            <a target="_blank" href="https://play.google.com/store/apps/details?id=kg.promarket" class="text-white text-decoration-none">Для Android (Google Play)</a>
          </li>
          <li>
            <a href="javascript:alert('Приложение для iOS в разработке');" class="text-white text-decoration-none">Для iOS (App Store)</a>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © 2021 Copyright:
    <a class="text-white" href="/">ProMarket</a> by <a class="text-secondary" href="https://besoft.kg" target="_blank">Besoft</a>
  </div>
</footer>
