<nav class="lg:fixed xl:fixed w-screen border-gray-200 border-b-2 bg-gray-100">
  <div class="container mt-1 mx-auto justify-between items-center flex flex-col lg:flex-row xl:flex-row">
    <a href="/" class="text-4xl flex items-center">
      <img
        class="w-16 h-16 inline"
        src="/assets/logo.png"
        height="30"
        alt="ProMarket"
      />
      <span class="text-secondary">PRO</span><span class="text-primary">MARKET</span>
    </a>

    <form method="get" class="flex-grow justify-center items-center flex" action="/search">
      <input
        type="search"
        class="p-2 w-60 pr-0 focus:outline-none rounded-l-lg shadow-2xl border-2 border-r-0"
        placeholder="Что вы ищете?"
        aria-label="Найти"
        name="query"
        value=""
      />
      <button
        class="bg-white p-2 rounded-r-lg hover:bg-green-50 text-primary border-2 border-l-0"
        type="submit"
      >
        Найти
      </button>
    </form>

    <div class="xl:mt-0 xl:mb-0 lg:mt-0 lg:mb-0 mt-5 mb-5">
      <ul class="flex justify-end flex-column">
        <li>
          <a class="p-2 border-2 bg-white m-1 rounded-full text-primary hover:text-green-500 hover:border-green-400"
             aria-current="page" href="/">Главная</a>
        </li>
        <li>
          <a class="p-2 border-2 bg-white m-1 rounded-full text-primary hover:text-green-500 hover:border-green-400"
             href="/discounts">Скидки</a>
        </li>
        <li>
          <a class="p-2 border-2 bg-white m-1 rounded-full text-primary hover:text-green-500 hover:border-green-400"
             href="/favorites" aria-disabled="true">Избранные</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="text-center">
    <div class="text-secondary inline-block p-2 rounded">Сайт в разработке...</div>
  </div>
</nav>
