@extends('layouts/app')

@section('meta_title', "Магазин " . $item->title . " в ProMarket")
@section('meta_description', "Посмотрите все товары, их цены и адреса магазина " . $item->title)

@if($item->logo)
  @section('meta_image', "https://api.office.promarket.besoft.kg/" . $item->logo->path['original'])
  @section('meta_image_type', "image/" . ($item->logo->extension === 'svg' ? $item->logo->extension . '+xml' : $item->logo->extension))
  @if($item->logo->dimensions['original']['width'])
    @section('meta_image_width', $item->logo->dimensions['original']['width'])
  @endif
  @if($item->logo->dimensions['original']['height'])
    @section('meta_image_height', $item->logo->dimensions['original']['height'])
  @endif
@endif

@section('content')

  <div class="grid xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-2">
    <div class="xl:mr-5 lg:mr-5 md:mr-5 mr-0 col-span-1 flex justify-center items-center">
      <img
        alt="{{"Магазин " . $item->title . " в ProMarket"}}"
        class="xl:w-3/4 lg:w-3/4 md:w-3/4 w-1/2"
        src="https://api.office.promarket.besoft.kg/{{$item->logo->path['original']}}">
    </div>
    <div class="xl:col-span-3 lg:col-span-3 md:col-span-1 m-5">
      <div class="text-3xl my-3 text-center md:text-left lg: text-left xl:text-left">{{$item->title}}</div>
      <div class="text-center md:text-left lg: text-left xl:text-left">{{$item->about}}</div>
      <div class="my-3 text-lg text-gray-500">Связь с магазином</div>
      @foreach($item->contacts as $c)
        @if ($c['type'] === 'phone')
          <a class="p-2 mr-2 border-2 rounded-full hover:border-primary"
             href="tel:+{{$c['value']}}">+{{$c['value']}}</a>
        @endif
        @if ($c['type'] === 'instagram')
          <a class="p-2 mr-2 border-2 rounded-full hover:border-primary" target="_blank"
             href="https://instagram.com/{{$c['value']}}">{{$c['value']}}</a>
        @endif
      @endforeach
    </div>
  </div>

  <h2 class="text-2xl pt-5 mx-2"
      style="width: 100%; border-bottom: 1px solid #eee; line-height: 0.1em; margin: 10px 0 20px;">
    <span class="px-2 bg-white text-gray-700">Товары магазина {{$item->title}}</span>
  </h2>

  <div class="grid sm:grid-cols-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
    @foreach($item->offers as $p)
      <x-offer-item :item="$p"/>
    @endforeach
  </div>

@endsection
