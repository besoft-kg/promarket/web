@extends('layouts/app')

@section('content')

  <div class="flex mb-2 flex-row flex-wrap">
    @foreach($main_categories as $c)
      <div>
        <a
          class="text-green-500 hover:border-gray-900 hover:text-gray-900 flex items-center p-1.5 border-2 m-1 rounded-full"
          href="/category/{{$c->id}}">
          <img width="20px" class="inline mr-2"
               src="https://api.office.promarket.besoft.kg/{{$c->icon->path['original']}}" alt="{{$c->title}}">
          <div class="inline">{{$c->short_title}}</div>
        </a>
      </div>
    @endforeach
  </div>

  <h2 class="text-2xl pt-3 mx-2"
      style="width: 100%; border-bottom: 1px solid #eee; line-height: 0.1em; margin: 10px 0 20px;">
    <span class="px-2 bg-white text-gray-700">Популярные товары</span>
  </h2>

  <div class="grid sm:grid-cols-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
    @foreach($products['popular'] as $p)
      <x-product-item :item="$p"/>
    @endforeach
  </div>

  <h2 class="text-2xl pt-5 mx-2"
      style="width: 100%; border-bottom: 1px solid #eee; line-height: 0.1em; margin: 10px 0 20px;">
    <span class="px-2 bg-white text-gray-700">Товары со скидкой</span>
  </h2>

  <div class="grid sm:grid-cols-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
    @foreach($products['discount'] as $p)
      <x-product-item :item="$p"/>
    @endforeach
  </div>

  @foreach($products['by_categories'] as $key => $p)
    <div class="flex flex-row">
      <img width="40px" height="40px" src="https://api.office.promarket.besoft.kg/{{$p->icon->path['original']}}"
           class="inline ml-3">
      <h2 class="text-2xl pt-5 mx-2"
          style="width: 100%; border-bottom: 1px solid #eee; line-height: 0.1em; margin: 10px 0 20px;">
        <span class="px-2 bg-white text-secondary">
          {{$p->title}}
        </span>
      </h2>
    </div>

    <div class="grid sm:grid-cols-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
      @foreach($p->products as $s)
        <x-product-item :item="$s"/>
      @endforeach
    </div>
  @endforeach

@endsection
