@extends('layouts/app')

@section('meta_title', $item->title . " в ProMarket")
@section('meta_description', "Сравнивайте цены на товар $item->title и купите на сайте ProMarket")
@section('meta_type', 'article')

@if(count($item->images) > 0)
  @section('meta_image', "https://api.office.promarket.besoft.kg/" . $item->images[0]->image->path['original'])
  @section('meta_image_type', "image/" . ($item->images[0]->image->extension === 'svg' ? $item->images[0]->image->extension . '+xml' : $item->images[0]->image->extension))
  @if($item->images[0]->image->dimensions['original']['width'])
    @section('meta_image_width', $item->images[0]->image->dimensions['original']['width'])
  @endif
  @if($item->images[0]->image->dimensions['original']['height'])
    @section('meta_image_height', $item->images[0]->image->dimensions['original']['height'])
  @endif
@endif

@section('content')

  <div class="px-2 pb-4 grid xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-1 grid-cols-1">
    <div class="items-center flex justify-center">
      <img alt="{{$item->title}}" class="max-h-80 md:max-h-full"
           src="{{count($item->images) > 0 ? 'https://api.office.promarket.besoft.kg/' . $item->images[0]->image->path['original'] : ''}}">
    </div>
    <div class="mx-5 col-span-2">
      <h2 class="text-3xl pt-3">
        <span class="bg-white text-gray-700 pr-3">{{$item->title}}</span>
      </h2>
      @if (count($item->colors) > 0)
        <div class="mt-3 text-lg text-gray-500">Цвета товара: <span class="text-black">не важно</span></div>
        @foreach($item->colors as $c)
          <div class="cursor-pointer w-8 rounded-full h-8 inline-block" title="{{$c->title}}"
               style="background-color: #{{$c->hex_code}}"></div>
        @endforeach
      @endif
      <div class="mt-3 text-lg text-gray-500">Коротко о товаре</div>
      <ul>
        @foreach($item->brief_info as $b)
          <li><span class="text-primary">•</span> <span class="ml-2">{{$b->value}}</span></li>
        @endforeach
      </ul>
      @if($item->brand)
        <a href="/brand/{{$item->brand->id}}" class="mt-5 flex items-center">
          <img class="w-6" src="https://api.office.promarket.besoft.kg/{{$item->brand->logo->path['original']}}">
          <span class="ml-3">Все товары бренда <strong
              class="hover:text-primary">{{$item->brand->title}}</strong></span>
        </a>
      @endif
    </div>
    <div class="md:col-span-3 lg:col-span-1 lg:mt-3 mt-5">
      <h3 class="text-lg">Предложения ({{count($item->offers)}})</h3>
      @if(count($item->offers) > 0)
        @foreach($item->offers as $offer)
          <a href="/offer/{{$offer->id}}">
            <div class="shadow transition-shadow hover:shadow-lg p-3 w-full my-3 rounded">
              <div class="flex items-center justify-between">
                <div class="text-2xl text-primary">
                  {{number_format($offer->sale_price, 0, '.', ' ')}} <u>с</u>
                  @if($offer->discount_active)
                    <span class="text-lg line-through text-gray-400">{{number_format($offer->price, 0, '.', ' ')}} <u>с</u></span>
                  @endif
                </div>
                <img src="https://api.office.promarket.besoft.kg/{{$offer->store->logo->path['original']}}"
                     class="w-10 inline-block">
              </div>

              <div class="my-1 text-gray-500">{{$offer->product->title}}</div>
              <div class="flex justify-between">
                <div>{{$offer->store->title}}</div>
                @if($offer->discount_active)
                  <div class="inline-block px-1 text-white bg-red-500 rounded-md">
                    Скидка {{$offer->discount_percentage}}%
                  </div>
                @endif
              </div>
            </div>
          </a>
        @endforeach
      @else
        <div class="shadow p-3 w-full my-3 rounded">
          <span class="text-red-500 text-lg">Товар пока что нет в продаже</span>
        </div>
      @endif
    </div>
  </div>

  {{--  {{dd($item)}}--}}

@endsection
