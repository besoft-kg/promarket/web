@extends('layouts/app')

@section('meta_title', "Поиск по запросу $query — ProMarket")
@section('meta_description', "Посмотрите результаты по запросу $query")
@section('meta_type', 'website')

@section('content')

  <div class="px-2 pb-4">
    <h2 class="text-3xl pt-5 mx-2"
        style="width: 100%; border-bottom: 1px solid #eee; line-height: 0.1em; margin: 10px 0 20px;">
      <span class="bg-white text-gray-700 pr-3">Результаты по запросу "{{$query}}"</span>
    </h2>

    @if (count($result['products']) === 0 && count($result['stores']) === 0 && count($result['categories']) === 0 && count($result['brands']) === 0)
      <p class="py-5 text-gray-500">Ничего не найдено. Попробуйте изменить запрос.</p>
      <h3 class="text-lg">Может вам интересно...</h3>
    @endif

    @if (count($result['products']) > 0)
      <h3 class="py-3 text-lg text-gray-600">Найденные товары ({{count($result['products'])}})</h3>

      <div class="grid sm:grid-cols-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
        @foreach($result['products'] as $s)
          <x-product-item :item="$s"/>
        @endforeach
      </div>
    @endif
  </div>

@endsection
