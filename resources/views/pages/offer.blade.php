@extends('layouts/app')

@section('meta_title', $item->product->title . " в " . $item->store->title . " — ProMarket")
@section('meta_description', "Посмотрите предложения магазина " . $item->store->title . " и купите на сайте ProMarket")
@section('meta_type', 'article')

@if(count($item->product->images) > 0)
  @section('meta_image', "https://api.office.promarket.besoft.kg/" . $item->product->images[0]->image->path['original'])
  @section('meta_image_type', "image/" . ($item->product->images[0]->image->extension === 'svg' ? $item->product->images[0]->image->extension . '+xml' : $item->product->images[0]->image->extension))
  @if($item->product->images[0]->image->dimensions['original']['width'])
    @section('meta_image_width', $item->product->images[0]->image->dimensions['original']['width'])
  @endif
  @if($item->product->images[0]->image->dimensions['original']['height'])
    @section('meta_image_height', $item->product->images[0]->image->dimensions['original']['height'])
  @endif
@endif

@section('content')

  <div class="px-2 pb-4 grid xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-1 grid-cols-1">
    <div class="items-center flex justify-center">
      <img alt="{{$item->product->title}} в {{$item->store->title}}" class="max-h-80 md:max-h-full"
           src="{{count($item->product->images) > 0 ? 'https://api.office.promarket.besoft.kg/' . $item->product->images[0]->image->path['original'] : ''}}">
    </div>
    <div class="mx-5 col-span-2">
      <h2 class="text-3xl pt-3">
        <span class="bg-white text-gray-700 pr-3">{{$item->product->title}} в <span
            class="text-secondary">{{$item->store->title}}</span></span>
      </h2>

      <div class="text-4xl mt-3 text-primary">
        {{number_format($item->sale_price, 0, '.', ' ')}} <u>с</u>
        @if($item->discount_active)
          <span class="text-xl line-through text-gray-400">{{number_format($item->price, 0, '.', ' ')}} <u>с</u></span>

          <div class="inline-block px-1 my-2 text-lg text-white bg-red-500 rounded-md">
            Скидка {{$item->discount_percentage}}%
          </div>
        @endif
      </div>

      <div class="my-3 text-lg text-gray-500">Связь с магазином</div>
      @foreach($item->store->contacts as $c)
        @if ($c['type'] === 'phone')
          <a class="p-2 mr-2 border-2 rounded-full hover:border-primary" href="tel:+{{$c['value']}}">+{{$c['value']}}</a>
        @endif
        @if ($c['type'] === 'instagram')
          <a class="p-2 mr-2 border-2 rounded-full hover:border-primary" target="_blank" href="https://instagram.com/{{$c['value']}}">{{$c['value']}}</a>
        @endif
      @endforeach

      <div class="mb-2 mt-4 text-lg text-gray-500">Коротко о товаре</div>
      <ul>
        @foreach($item->product->brief_info as $b)
          <li><span class="text-primary">•</span> <span class="ml-2">{{$b->value}}</span></li>
        @endforeach
      </ul>
      <a href="/store/{{$item->store->id}}" class="mt-5 flex items-center">
        <img class="w-6" src="https://api.office.promarket.besoft.kg/{{$item->store->logo->path['original']}}">
        <span class="ml-3">Все товары магазина <strong
            class="hover:text-primary">{{$item->store->title}}</strong></span>
      </a>
      @if($item->product->brand)
        <a href="/brand/{{$item->product->brand->id}}" class="mt-2 flex items-center">
          <img class="w-6"
               src="https://api.office.promarket.besoft.kg/{{$item->product->brand->logo->path['original']}}">
          <span class="ml-3">Все товары бренда <strong
              class="hover:text-primary">{{$item->product->brand->title}}</strong></span>
        </a>
      @endif
    </div>
    <div class="md:col-span-3 lg:col-span-1 lg:mt-3 mt-5">
      <a href="/store/{{$item->store->id}}" class="flex items-center justify-center my-5">
        <img class="w-1/2" src="https://api.office.promarket.besoft.kg/{{$item->store->logo->path['original']}}">
      </a>
      <h3 class="text-lg">Где купить?</h3>
      @foreach($item->branches as $branch)
        <div class="shadow transition-shadow hover:shadow-lg p-3 w-full my-3 rounded">
          <div class="text-gray-700">{{$branch->address}}</div>
        </div>
      @endforeach
    </div>
  </div>

  {{--  {{dd($item)}}--}}

@endsection
