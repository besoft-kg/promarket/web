<div class="m-1">
  <a href="/offer/{{$item->id}}"
     class="hover:border-gray-300 border-gray-100 flex justify-between flex-col h-full rounded-lg border-2 p-2">
    <div>
      <div
        style="width: 100%; height: 180px; background-repeat: no-repeat; background-position: center; background-origin: content-box; background-size: contain; padding: 18px; background-image: url('{{$item->product->main_image ? 'https://api.office.promarket.besoft.kg/' . $item->product->main_image->path['original'] : ''}}')"></div>
      <h6 class="text-gray-700" title="{{$item->product->title}}"
          style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{{$item->product->title}}</h6>
    </div>
    <div>
      <h5 class="text-primary text-2xl">
        {{number_format($item->sale_price, 0, '.', ' ')}} <u>с</u>
        @if ($item->discount_active)
          <span class="text-xl line-through text-gray-500">{{number_format($item->price, 0, '.', ' ')}} <u>с</u></span>
        @endif
      </h5>
    </div>
  </a>
</div>
