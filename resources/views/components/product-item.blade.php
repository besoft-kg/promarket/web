<div class="m-1">
  <a href="/product/{{$item->id}}"
     class="hover:border-gray-300 border-gray-100 flex justify-between flex-col h-full rounded-lg border-2 p-2">
    <div>
      <div
        style="width: 100%; height: 180px; background-repeat: no-repeat; background-position: center; background-origin: content-box; background-size: contain; padding: 18px; background-image: url('{{$item->main_image ? 'https://api.office.promarket.besoft.kg/' . $item->main_image->path['original'] : ''}}')"></div>
      <h6 class="text-gray-700" title="{{$item->title}}" style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{{$item->title}}</h6>
    </div>
    <div>
      @if ($item->cheapest_offer)
        <h5 class="text-primary text-2xl">
          <span class="text-gray-700 text-xl">от</span>
          {{number_format($item->cheapest_offer->sale_price, 0, '.', ' ')}} <u>с</u>
          @if ($item->cheapest_offer->discount_active)
            <span class="text-xl line-through text-gray-500">{{number_format($item->cheapest_offer->price, 0, '.', ' ')}} <u>с</u></span>
          @endif
        </h5>
      @else
        <h5 class="text-xl text-gray-400">Нет в продаже</h5>
      @endif
      <p class="text-sm text-gray-500">Предложения: {{$item->offers_count}}</p>
    </div>
  </a>
</div>
