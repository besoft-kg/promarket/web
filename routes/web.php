<?php

use App\Models\Category;
use App\Models\IBrand;
use App\Models\IProduct;
use App\Models\Product;
use App\Models\SearchQuery;
use App\Models\Store;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  $products = [
    'by_categories' => Category::whereNull('parent_id')->whereRaw(DB::raw(
        'EXISTS(SELECT 1 FROM tabekg_products WHERE category_id = tabekg_categories.id OR JSON_CONTAINS(tabekg_categories.all_subcategories, CONVERT(tabekg_products.category_id, CHAR), \'$\'))'
      ))->get(),
    'popular' => IProduct::whereNull('parent_id')
      ->whereRaw(DB::raw(
        'EXISTS(SELECT id FROM tabekg_offers WHERE (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE tabekg_products.id = p.parent_id)) AND EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id))'
      ))
      ->limit(10)
      ->orderByRaw('RAND()')
      ->get(),
    'discount' => IProduct::whereNull('parent_id')->whereRaw(DB::raw(
      'EXISTS(SELECT id FROM tabekg_offers WHERE (tabekg_offers.product_id = tabekg_products.id OR tabekg_offers.product_id IN(SELECT id FROM tabekg_products as p WHERE tabekg_products.id = p.parent_id)) AND (CURRENT_DATE BETWEEN discount_start_on AND discount_end_on) AND EXISTS(SELECT id FROM tabekg_offer_branches WHERE tabekg_offer_branches.offer_id = tabekg_offers.id))'
    ))->limit(10)->orderBy('id', 'desc')->get(),
  ];

  return view('pages/index', [
    'products' => $products,
  ]);
});

Route::get('/privacy', function () {
  return view('pages/privacy');
});

Route::get('/search', function (\Illuminate\Http\Request $request) {

  $query = $request->get('query');

  if (!$request->has('query') || strlen($query) === 0) {
    return redirect('/');
  }

  $query_words = explode(' ', $query);
  $ip = $request->ip();

  $result = [
    'products' => IProduct::whereNull('parent_id')->where(function($q) use ($query, $query_words){
      $q->where('title', 'like', '%' . $query . '%');
      foreach ($query_words as $s) {
        $q->orWhereRaw('id IN(SELECT product_id FROM tabekg_product_keywords WHERE text = ?)', [$s]);
      }
    })->get(),
    'categories' => Category::where(function($q) use ($query, $query_words){
      $q->where('title', 'like', '%' . $query . '%');
      foreach ($query_words as $s) {
        $q->orWhereRaw('id IN(SELECT category_id FROM tabekg_category_keywords WHERE text = ?)', [$s]);
      }
    })->get(),
    'brands' => IBrand::where('title', 'like', '%' . $query . '%')->get(),
    'stores' => Store::where('title', 'like', '%' . $query . '%')->get(),
  ];

  $search_query = SearchQuery::where('user_ip', $ip)->where('query', $query)->first();

  if ($search_query) {
    $search_query->total++;
    $search_query->save();
  } else {
    SearchQuery::create([
      'user_ip' => $request->ip(),
      'query' => $query,
    ]);
  }

  return view('pages/search', [
    'query' => $query,
    'result' => $result,
  ]);

});

Route::get('/product/{id}', function (\Illuminate\Http\Request $request, $id) {

  $item = Product::findOrFail($id)->append('offers');

  return view('pages/product', [
    'item' => $item,
  ]);

});

Route::get('/offer/{id}', function (\Illuminate\Http\Request $request, $id) {

  $item = \App\Models\Offer::findOrFail($id);

  return view('pages/offer', [
    'item' => $item,
  ]);

});

Route::get('/store/{id}', function (\Illuminate\Http\Request $request, $id) {

  $item = \App\Models\Store::findOrFail($id);

  return view('pages/store', [
    'item' => $item,
  ]);

});
