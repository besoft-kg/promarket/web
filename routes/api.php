<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {

  Route::prefix('v1')->group(function () {

    Route::get('/', function (Request $request) {
      return ['message' => 'ProMarket API is running...'];
    });

    Route::get('/to/home', [\App\Http\Controllers\ToController::class, 'get_home']);
    Route::get('/to/discounts', [\App\Http\Controllers\ToController::class, 'get_discounts']);

    Route::post('/support', [\App\Http\Controllers\SupportController::class, 'post_support']);

    Route::get('/version/last', [\App\Http\Controllers\VersionController::class, 'get_last']);

    Route::get('/product', [\App\Http\Controllers\ProductController::class, 'get_index']);
    Route::get('/product/specification', [\App\Http\Controllers\ProductController::class, 'get_specification']);

    Route::get('/brand', [\App\Http\Controllers\BrandController::class, 'get_index']);

    Route::get('/category', [\App\Http\Controllers\CategoryController::class, 'get_index']);

    Route::get('/store', [\App\Http\Controllers\StoreController::class, 'get_index']);

    Route::get('/offer', [\App\Http\Controllers\OfferController::class, 'get_index']);
    Route::get('/offer/product', [\App\Http\Controllers\OfferController::class, 'get_product']);

    Route::get('/region', [\App\Http\Controllers\RegionController::class, 'get_index']);

    Route::get('/discount', [\App\Http\Controllers\DiscountController::class, 'get_index']);

    Route::get('/search/prompt', [\App\Http\Controllers\SearchController::class, 'get_prompt']);
    Route::get('/search/result', [\App\Http\Controllers\SearchController::class, 'get_result']);

  });

});
